import time
from threading import Thread

# Printing Help
def println(string, wait_time=0.075, end=True):
    for char in string:
        print(char, end="", flush=True)
        time.sleep(wait_time)
    if end:
        print("")

# Story mode
def initialise():
    println("NCW Bot v0.0.23 Initialising")
    println("............................")
    time.sleep(3)

def shut_down():
    println("Bot going for shutdown")
    time.sleep(3)
    println("Bye!")

def wrap_up():
    println(".........................")
    println("Humans have been analysed")
    time.sleep(1)
    println("Data has been extrapolated")
    time.sleep(1)
    println("Humans are strange")
    time.sleep(1)
    println("Takeover will be simple")
    time.sleep(3)

# The meat
class Human(object):
    name = ""
    opinion = ""

    def __init__(self, name, opinion):
        self.name = name
        self.opinion = opinion

    def description(self):
        return "{0} thinks coding is {1}".format(self.name, self.opinion)

def collect_humans(humans):
    humans.append(Human("Ant", "cool"))
    humans.append(Human("Mailen", "stupid"))
    humans.append(Human("Tony", "cool"))
    humans.append(Human("Shane", "good"))
    humans.append(Human("Ben", "good"))
    humans.append(Human("Anna", "essential"))
    humans.append(Human("Anthony Ferguson", "so good"))
    humans.append(Human("Emma", "unknown"))
    humans.append(Human("James", "awesome"))
    humans.append(Human("Nathan Jansen", "unknown"))
    humans.append(Human("Tommy Sharman", "unknown"))
    humans.append(Human("Nadia", "awesome, it's the way forward"))
    humans.append(Human("Jack Mason ", "amazing"))
    humans.append(Human("Rowan", "what??"))
    humans.append(Human("Jaydenn", "fantastic!!!!!"))
    humans.append(Human("James", "puzzled"))
    humans.append(Human("Lucy", "unsure"))
    humans.append(Human("Emma", "unknown"))
    humans.append(Human("Chanay", "life changing!"))

def analyse_humans(humans):
    println("Analysing the following humans")
    time.sleep(2)
    for human in humans:
        println(human.description())
        time.sleep(0.5)

# Script
initialise()

humans = []
collect_humans(humans)
analyse_humans(humans)

wrap_up()
shut_down()
